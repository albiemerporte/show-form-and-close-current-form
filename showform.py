
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication

class Form1:
    def __init__(self):
        self.form1 = loadUi('form1.ui')
        self.form1.show()
        self.extform2 = Form2()
        self.form1.pbShowForm2.clicked.connect(self.showform2)
        
    def showform2(self):
        self.form1.close()
        self.extform2.form2.show()

class Form2:
    def __init__(self):
        self.form2 = loadUi('form2.ui')
        self.form2.pbShowForm1.clicked.connect(self.showform1)
        
    def showform1(self):
        self.extform1 = Form1()
        self.form2.close()
        self.extform1.form1.show()
        
        

if __name__ == '__main__':
    app = QApplication([])
    main = Form1()
    app.exec()